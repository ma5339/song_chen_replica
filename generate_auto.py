def run():
    count = 0
    for pooling in ["sum", "mean", "max"]:#--context_pooling
        for aggregation in ["last", "sum", "max"]:#--JK
            for decay in [0, 0.1, 0.001]:#--decay
                for dropout in [0,0.2,0.4]:#--dropout_ratio
                    params = f"--context_pooling {pooling} --JK {aggregation} --decay {decay} --dropout_ratio {dropout}"
                    name = f"TUNN_{count}_"
                    #print(f"python3 pretrain.py --dataset data_from_old/ARQMath_1-3_qrels_tunning/full.pt {params} --initial_node emb --name {name} --num_node_label 1647 --num_edge_label 56 --epochs 100")
                    print(f"python3 graph_rep.py --JK {aggregation} --num_node_label 1648 --num_edge_label 56 --initial_node emb --input_model_file model/{name}1648.pth --dataset data_from_old/ARQMath_1-3_qrels_tunning/full.pt --tail {name}ARQMath_1-3_qrels_tunning")
                    print(f"python3 graph_rep.py --JK {aggregation} --num_node_label 1648 --num_edge_label 56 --initial_node emb --input_model_file model/{name}1648.pth --dataset data_from_old/ARQMath1_topics_OPT_MERGE_from_prebuilt_index/full.pt --tail {name}ARQMath1_topics_OPT_MERGE")
                    print(f"python3 retrieval_main.py -d graph_rep/{name}ARQMath_1-3_qrels_tunning -p graph_rep/{name}ARQMath1_topics_OPT_MERGE -k 1000 -n {name}")                    
                    #
                    count+=1                    
    
run()
print(f"python3 metrics.py -q data/ARQMath_qrels/qrel_official_v_2.tsv -p result/ -r grid_search_results/all_results.tsv")