
import argparse
import torch
import os
import json
import csv
from tqdm import tqdm

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--dataset_namespace', type=str)
    parser.add_argument('-p','--topics_namespace', type=str)    
    parser.add_argument('-k','--top_k', type=int, default=1000)
    parser.add_argument('-n','--name', type=str)
    parser.add_argument('-o','--out_path', type=str, default="result_grid_all_reps")

    

    args = parser.parse_args()

    name = args.name
    top_k = args.top_k
    dataset_namespace = args.dataset_namespace
    topics_namespace = args.topics_namespace
    out_path = args.out_path

    if not os.path.exists(out_path):
        os.makedirs(out_path)


    embedding_dataset = torch.load(os.path.join(dataset_namespace,"full.pt"))
    with open(os.path.join(dataset_namespace,"index.json")) as f:
        embedding_index = json.load(f)

    full_dataset_tensor = embedding_dataset[0]
    for tensor in embedding_dataset[1:]:
        full_dataset_tensor = torch.cat([full_dataset_tensor, tensor], dim=0)

    embedding_topics = torch.load(os.path.join(topics_namespace,"full.pt"))
    with open(os.path.join(topics_namespace,"index.json")) as f:
        topics_index = json.load(f)

    full_topics_tensor = embedding_topics[0]
    for tensor in embedding_topics[1:]:
        full_topics_tensor = torch.cat([full_topics_tensor, tensor], dim=0)
    
    results = []
    for _idx, topic in tqdm(enumerate(topics_index)):
        
        topic_number = topic.split(":")
        if int(topic_number[0]) > 20:
            continue
        topic_name = f"NTCIR12-MathWiki-{topic_number[0]}"
        #topic_name = f"{topic_number[0]}"
        topic_vector = full_topics_tensor[_idx]
        cos_tensor = torch.cosine_similarity(topic_vector, full_dataset_tensor, dim=-1)
        values, indices = torch.topk(cos_tensor, top_k, largest=True, sorted=True)
        #temp = NTCIR_ID + ' xxx ' + A + ' ' + str(j+1) + ' ' + B + ' ' + "slt_no_merge_100_epoch"
        for j in range(top_k):
            retrieved_formula_id = embedding_index[indices[j]]
            score = values[j].item()
            results.append([topic_name, retrieved_formula_id, retrieved_formula_id, j+1, score, name])
    with open(os.path.join(out_path, f"{name}.tsv"), "w+") as f:
        writer = csv.writer(f, delimiter="\t", quotechar='"')        
        writer.writerows(results)
    print("Retrieval done")


if __name__ == "__main__":
    main()
