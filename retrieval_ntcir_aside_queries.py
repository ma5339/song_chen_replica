import json
import torch
from glob import glob
import os
from tqdm import tqdm

top_k=1000

embedding_path = "graph_rep/graph_rep_original_corrected_ids/full.pt"
path_individual_files = "data_processed/full_ntcir_corrected_ids/torch_data/*"
ids_formulas = list(map(lambda x: os.path.basename(x)[:-3],glob(path_individual_files)))
ids_formulas.sort()

embedding_path_topics = "graph_rep/graph_rep_ntcir_queries_original_model/full.pt"
path_individual_files_topics = "data_processed/ntcir_queries/torch_data"
ids_topics = list(map(lambda x: os.path.basename(x)[:-3],glob(f"{path_individual_files_topics}/*")))
ids_topics.sort()


t = open('result/results_corrected_ids.txt', 'w+', encoding='utf-8')

all_embedding = torch.load(embedding_path)
full_graph_rep_tensor = all_embedding[0]
for tensor in all_embedding[1:]:
    full_graph_rep_tensor = torch.cat([full_graph_rep_tensor, tensor], dim=0)

all_embedding_topics = torch.load(embedding_path_topics)
full_graph_rep_tensor_topics = all_embedding_topics[0]
for tensor in all_embedding_topics[1:]:
    full_graph_rep_tensor_topics = torch.cat([full_graph_rep_tensor_topics, tensor], dim=0)


#import pdb;pdb.set_trace()
for _idx, topic in tqdm(enumerate(ids_topics)):    
    if int(topic)>20:
        continue
    torch_query = full_graph_rep_tensor_topics[_idx]
        
    NTCIR_ID = f"NTCIR12-MathWiki-{topic}"    

    query_rep = torch_query

    cos_tensor = torch.cosine_similarity(query_rep, full_graph_rep_tensor, dim=-1)
    values, indices = torch.topk(cos_tensor, top_k, largest=True, sorted=True)
    #import pdb; pdb.set_trace()
    for j in range(top_k):        
        try:
            A = ids_formulas[indices[j]]
            B = str(values[j].item())
            temp = NTCIR_ID + ' xxx ' + A + ' ' + str(j+1) + ' ' + B + ' ' + "corrected_ids"
        except:
            import pdb; pdb.set_trace()
        t.write(temp + '\n')    
t.close()