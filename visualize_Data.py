import networkx as nx
import torch
import torch_geometric
import matplotlib
import matplotlib.pyplot

import sys

delta=5
allowable_features = torch.load('w2c/words_mc=' + str(delta) + '.pt')

filename = sys.argv[1]
data_torch = torch.load(filename)[0]
g = torch_geometric.utils.to_networkx(data_torch)
pos = nx.shell_layout(g)
edges = g.edges()

dict_labels = {}

for idx,edge in enumerate(data_torch.edge_index.T):
    from_, to_ = edge
    dict_labels[(from_.item(), to_.item())] = data_torch.edge_attr[idx].item()



fig = matplotlib.pyplot.figure()

nx.draw_networkx(
    g, pos, edge_color='black', width=1, linewidths=1,
    node_size=500, node_color='pink', alpha=0.9,
    labels={node: allowable_features[data_torch.x[idx].item()] for idx,node in enumerate(g.nodes())}
)

nx.draw_networkx_edge_labels(
    g, pos,
    edge_labels=dict_labels,
    font_color='red'
)
matplotlib.pyplot.show()