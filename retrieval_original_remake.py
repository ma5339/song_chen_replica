import json
import torch
from glob import glob
import os
from tqdm import tqdm


queries = [[374354, "data_processed/full_ntcir_song_chen/formulas_graph/Prime_zeta_function:28.txt"],
[73889, "data_processed/full_ntcir_song_chen/formulas_graph/Codex_Sinaiticus:1.txt"],
[518779, "data_processed/full_ntcir_song_chen/formulas_graph/Word_lists_by_frequency:1.txt"],
[338468, "data_processed/full_ntcir_song_chen/formulas_graph/On_Physical_Lines_of_Force:0.txt"],
[29058, "data_processed/full_ntcir_song_chen/formulas_graph/Backhouse's_constant:0.txt"],
[498515, "data_processed/full_ntcir_song_chen/formulas_graph/Unbinilium:1.txt"],
[155252, "data_processed/full_ntcir_song_chen/formulas_graph/Extension_of_a_topological_group:38.txt"],
[335241, "data_processed/full_ntcir_song_chen/formulas_graph/Nowcast_(Air_Quality_Index):1.txt"],
[187471, "data_processed/full_ntcir_song_chen/formulas_graph/Generator_(circuit_theory):0.txt"],
[264035, "data_processed/full_ntcir_song_chen/formulas_graph/Lerch_zeta_function:0.txt"],
[312529, "data_processed/full_ntcir_song_chen/formulas_graph/Monic_polynomial:1.txt"],
[217722, "data_processed/full_ntcir_song_chen/formulas_graph/Hunt–McIlroy_algorithm:2.txt"],
[293254, "data_processed/full_ntcir_song_chen/formulas_graph/Mathematical_morphology:24.txt"],
[219484, "data_processed/full_ntcir_song_chen/formulas_graph/Hyperbolic_law_of_cosines:2.txt"],
[461960, "data_processed/full_ntcir_song_chen/formulas_graph/Strong_antichain:0.txt"],
[107795, "data_processed/full_ntcir_song_chen/formulas_graph/Delay_spread:2.txt"],
[192144, "data_processed/full_ntcir_song_chen/formulas_graph/Goldbach–Euler_theorem:7.txt"],
[455494, "data_processed/full_ntcir_song_chen/formulas_graph/Statistical_coupling_analysis:1.txt"],
[18368, "data_processed/full_ntcir_song_chen/formulas_graph/Anisotropic_Network_Model:4.txt"],
[93803, "data_processed/full_ntcir_song_chen/formulas_graph/Correlation_and_dependence:1.txt"]]


embedding_path = "graph_rep/graph_rep_baseline_emb_new/full.pt"
top_k=1000

path = f"data_processed/full_ntcir_song_chen/formulas_graph/*.txt"

ids_formulas = list(map(lambda x: os.path.basename(x)[:-4],glob(path)))
ids_formulas.sort()

t = open('result/baseline_test_new_trained.txt', 'w+', encoding='utf-8')

all_embedding = torch.load(embedding_path)
full_graph_rep_tensor = all_embedding[0]
for tensor in all_embedding[1:]:
    full_graph_rep_tensor = torch.cat([full_graph_rep_tensor, tensor], dim=0)
#import pdb;pdb.set_trace()
for idx_query,topic in tqdm(enumerate(queries)):    

    index_in_file, _ = topic
    
    NTCIR_ID = f"NTCIR12-MathWiki-{idx_query+1}"

    index_full_torch = index_in_file

    query_rep = full_graph_rep_tensor[index_full_torch]
    cos_tensor = torch.cosine_similarity(query_rep, full_graph_rep_tensor, dim=-1)
    values, indices = torch.topk(cos_tensor, top_k, largest=True, sorted=True)
    #import pdb; pdb.set_trace()
    for j in range(top_k):        
        try:
            A = ids_formulas[indices[j]]
            B = str(values[j].item())
            temp = NTCIR_ID + ' xxx ' + A + ' ' + str(j+1) + ' ' + B + ' ' + "baseline_test"
        except:
            import pdb; pdb.set_trace()
        t.write(temp + '\n')    
t.close()