import os
from math_tan.math_extractor import MathExtractor
from math_tan.math_document import MathDocument
from glob import glob
from tqdm import tqdm

path = "./data/NTCIR-12_MathIR_Wikipedia_Corpus/TestQueries/*"
destination = "./data_processed/ntcir_queries/formulas_text"

if not os.path.exists(destination):
    os.makedirs(destination)

def create_formula_string(filePathForresults, filePath, missing_tags=None, problem_files=None):
    files = glob(filePath)
   
    for file in tqdm(files):
        query_name = os.path.basename(file)[:-5]
        (ext, content) = MathDocument.read_doc_file(file)
        formulas = MathExtractor.parse_from_xml(content, 1, operator=True, missing_tags=missing_tags,
                                                    problem_files=problem_files)
        
        f = open(filePathForresults+"/"+query_name+".txt", "w+", encoding='utf-8')
        #import pdb;pdb.set_trace()
        try:
            f.write(formulas[0].tostring())
        except Exception as sse:
            print("---create_formula_string---",filePath, sse)
        f.close()


create_formula_string(destination, path)