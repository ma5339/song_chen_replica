import os
from math_tan.math_extractor import MathExtractor
from math_tan.math_document import MathDocument
from glob import glob
from bs4 import BeautifulSoup
from tqdm import tqdm

path = "./data/NTCIR-12_MathIR_Wikipedia_Corpus/MathTagArticlesExtracted/*/*.html"
destination = "./culo"


def create_formula_string(filePathForresults, filePath, missing_tags=None, problem_files=None):
    files = glob(filePath)

    file_original = "ids_antes.txt"
    file_mahtml = "ids_mathml.txt"

    f1 = open(file_original, "w+", encoding='utf-8')
    f2 = open(file_mahtml, "w+", encoding='utf-8')
    f3 = open("logs.txt", "w+", encoding='utf-8')
    
    

    for file in tqdm(files):
        try:
            parts = file.split('/')
            file_name = os.path.splitext(parts[len(parts)-1])[0]
            (ext, content) = MathDocument.read_doc_file(file)
            formulas = MathExtractor.parse_from_xml(content, 1, operator=True, missing_tags=missing_tags,
                                                        problem_files=problem_files)
            



            for key in formulas:
                pinga = formulas[key].xml_root            
                soup = BeautifulSoup(pinga, "xml")
                id_in_mathml = soup.find("math").get('id')
                id_formula = file_name+":"+str(key)
                f1.write(id_formula+'\n')
                f2.write(id_in_mathml+'\n')
        except Exception as E:
            f3.write(f"{file} - {E}")
            
            #print(E)

    f1.close()
    f2.close()    
    f3.close()   

create_formula_string(destination, path)