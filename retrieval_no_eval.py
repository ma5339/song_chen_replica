import json
import torch
from glob import glob
import os
from tqdm import tqdm


embedding_path = "graph_rep/graph_rep_original_song_chen/full.pt"
top_k=1000

path = f"data_processed/full_song_chen/torch_data/*"

topics_path = "graph_rep/graph_rep_topics_arq_original_song_model/full.pt"

temp = glob(path)
temp.sort()
ids_formulas = list(map(lambda x: os.path.basename(x)[:-3],temp))#-4 para lo demas, -3 solo original
if "full" in ids_formulas:
    ids_formulas.remove("full")


all_embedding = torch.load(embedding_path)
full_graph_rep_tensor = all_embedding[0]
for tensor in all_embedding[1:]:
    full_graph_rep_tensor = torch.cat([full_graph_rep_tensor, tensor], dim=0)

all_topics = torch.load(topics_path)

topics_path_individuals = "data_processed/topics_arqmath/torch_data/*"
temp = glob(topics_path_individuals)
temp.sort()
ids_topics = list(map(lambda x: os.path.basename(x)[:-3],temp))#-4 para lo demas, -3 solo original

if "full" in ids_topics:
    ids_topics.remove("full")

full_topics_tensor = all_topics[0]
for tensor in all_topics[1:]:
    full_topics_tensor = torch.cat([full_topics_tensor, tensor], dim=0)

t = open('result/original_song_chen.txt', 'w+', encoding='utf-8')
#import pdb;pdb.set_trace()
for _idx,topic in tqdm(enumerate(full_topics_tensor)):    


    cos_tensor = torch.cosine_similarity(topic, full_graph_rep_tensor, dim=-1)
    values, indices = torch.topk(cos_tensor, top_k, largest=True, sorted=True)
    #import pdb; pdb.set_trace()
    for j in range(top_k):        
        A = ids_formulas[indices[j]]
        
        name = A
        rfind = name.rfind('_')
        id = name[0:rfind] + ':' + name[rfind + 1:]


        B = str(values[j].item())
        #temp = f"{ids_topics[_idx]}" + ' xxx ' + A + ' ' + str(j+1) + ' ' + B + ' ' + "original_song_chen"
        temp = f"{ids_topics[_idx]}" + ' xxx ' + id + ' ' + str(j+1) + ' ' + B + ' ' + "original_song_chen"
        #temp = f"{ids_topics[_idx]}\t0\t{A}\t1.0"        
        t.write(temp + '\n')    
t.close()