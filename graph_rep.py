import argparse
import torch
from torch_geometric.data import DataLoader
from torch_geometric.nn import global_add_pool, global_mean_pool, global_max_pool
from model import GNN
import os
import json
from glob import glob

def get_num_labels(dataset):
    symbols_frequency_path = os.path.join(dataset, "symbols_frequency_index.json")
    #relations_frequency_path = os.path.join(dataset, "relations_frequency_index.json")
    #symbols_path = os.path.join(dataset, "symbols_index.json")
    relations_path = os.path.join(dataset, "relations_index.json")

    if not os.path.exists(symbols_frequency_path) or not os.path.exists(relations_path):
        return 5000, 62

    with open(symbols_frequency_path) as f:
        symbols_frequency = json.load(f)
    
    with open(relations_path) as f:
        relations = json.load(f)

    #num_node_label, num_edge_label
    return sum(i>=5 for i in symbols_frequency.values()), len(relations)


def pool_func(x, batch, mode="max"):
    if mode == "sum":
        return global_add_pool(x, batch)
    elif mode == "mean":
        return global_mean_pool(x, batch)
    elif mode == "max":
        return global_max_pool(x, batch)


def main():
    torch.manual_seed("0")
    # settings
    parser = argparse.ArgumentParser(description='Generating graph representations')
    parser.add_argument('--device', type=int, default=0,
                        help='which gpu to use if any (default: 0)')
    parser.add_argument('--batch_size', type=int, default=32,
                        help='input batch size for training (default: 32)')
    parser.add_argument('--num_layer', type=int, default=5,
                        help='number of GNN message passing layers (default: 5).')
    parser.add_argument('--emb_dim', type=int, default=300,
                        help='embedding dimensions (default: 300)')
    parser.add_argument('--graph_pooling', type=str, default="mean",
                        help='graph level pooling (sum, mean, max, set2set, attention)')
    parser.add_argument('--JK', type=str, default="last",
                        help='how the node features across layers are combined. last, sum, max or concat')
    parser.add_argument('--dropout_ratio', type=float, default=0, help='dropout ratio (default: 0)')
    parser.add_argument('--gnn_type', type=str, default="gcn")
    #parser.add_argument('--num_node_label', type=int, default=5000, help='number of node type or node feature')
    parser.add_argument('--input_model_file', type=str, default='gcn_5000_w2c.pth',
                        help='filename to read the model (if there is any)')
    parser.add_argument('--initial_node', type=str, default='emb', help='w2c or emb')
    parser.add_argument('--dataset', type=str, default='dataset/dataset_5000', help='dataset to be choosed')
    parser.add_argument('--tail', type=str, default='gcn_5000_w2c', help='filename tail')
    parser.add_argument('--get_sizes_from', type=str, default='muerte', help='get_sizes_from')

    #parser.add_argument('--num_edge_label', type=int, help='edge types')
    
    args = parser.parse_args()
    num_node_label, num_edge_label = get_num_labels(args.get_sizes_from)
    device = torch.device("cuda:" + str(args.device)) if torch.cuda.is_available() else torch.device("cpu")
    #device = torch.device("cpu")
    print(device)

    model = GNN(args.num_layer, args.emb_dim, JK=args.JK, drop_ratio=args.dropout_ratio,
                gnn_type=args.gnn_type, num_node_type=num_node_label, num_bond_type=num_edge_label,
                word2vec=args.initial_node, device=device).to(device)
    if not args.input_model_file == "":
        model.from_pretrained(args.input_model_file)
    model.to(device)



    full_graph_rep_list = []

    #for k in range(1, 17):

    #print(args.dataset + '/' + str(k) + ' is being loaded...')
    dataset = torch.load(os.path.join(args.dataset,"full.pt"))

    if "formula_id" in dataset[0]:
        index = [data.formula_id for data in dataset]    
    else:
        path = os.path.join(args.dataset,"torch_data/*")
        a = glob(path)
        a.sort()
        index = [os.path.basename(path)[:-3] for path in a]
    
    test_loader = DataLoader(dataset, batch_size=args.batch_size, shuffle=False, num_workers=0)    
    print('graph representations is being generated...')
    graph_rep_list = []
    for idx, batch in enumerate(test_loader):        
        model.eval()
        batch = batch.to(device)
        try:
            with torch.no_grad():
                batch.x[batch.x>=num_node_label] = 0
                batch.edge_attr[batch.edge_attr>=num_edge_label] = 0
                node_rep = model(batch.x, batch.edge_index, batch.edge_attr)
        except Exception as e:
            print(e)
            import pdb;pdb.set_trace()
        graph_rep = pool_func(node_rep, batch.batch, mode="mean")
        graph_rep_list.append(graph_rep)
    full_graph_rep_list = full_graph_rep_list + graph_rep_list
    #torch.save(graph_rep_list, 'graph_rep/graph_rep_' + args.tail + '/' + str(k) + '.pt')
    if not os.path.exists(os.path.join("graph_rep",args.tail)):
        os.makedirs(os.path.join("graph_rep",args.tail))
    torch.save(full_graph_rep_list, 'graph_rep/' + args.tail + '/' + 'full.pt')
    with open(os.path.join("graph_rep",args.tail,"index.json"),"w+") as f:
        f.write(json.dumps(index))


if __name__ == "__main__":
    main()
