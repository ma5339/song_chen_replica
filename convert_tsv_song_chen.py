
from itertools import repeat
import csv
from utils.map_reduce import combine_lists, map_reduce
from math_tan.math_extractor import MathExtractor
import os
from tqdm import tqdm

def mio_format_to_formula_string():
    #expects 
    path = "data/ARQMath_1-3_qrels/1.tsv"
    out = "data_processed/ARQMath_1-3_qrels/formulas_text"
    if not os.path.exists(out):
        os.makedirs(out)
    with open(path) as fd:
        rd = csv.reader(fd, delimiter="\t", quotechar='"')
        next(rd, None) #skipping header

        input_list = list(zip(list(rd),repeat(out)))
        #map_reduce(process_row, combine_lists, input_list)
        for data in tqdm(input_list): 
            process_row(data)
    
def process_row(data):
    row, out = data
    formula_id = row[0]
    opt_string = row[2]
    try:
        formulas = MathExtractor.parse_from_xml(opt_string, 1, operator=False, missing_tags=None,
                                                problem_files=None)
        f = open(f"{out}/{formula_id}.txt", "w+", encoding='utf-8')
    
        f.write(formulas[0].tostring())
        f.close()
    except Exception as e:
        print("mierda xd", formula_id, e)
    
    return []

def main():
    mio_format_to_formula_string()

if __name__ == "__main__":    
    main()
