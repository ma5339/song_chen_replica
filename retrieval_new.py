import json
import torch
from glob import glob
import os
from tqdm import tqdm


embedding_path = "graph_rep/graph_rep_slt_no_merge_22_duplicates_topics/full.pt"
top_k=1000


namespace = "data_from_old/ntcir_full_duplicates_slt_no_merge"
topics_path = f"{namespace}/topics_index.json"
path = f"{namespace}/individual/*"

ids_formulas = list(map(lambda x: os.path.basename(x)[:-4],glob(path)))


with open(topics_path, 'r') as f:
    topics = json.load(f)

t = open('result/porno.txt', 'w+', encoding='utf-8')

all_embedding = torch.load(embedding_path)
full_graph_rep_tensor = all_embedding[0]
for tensor in all_embedding[1:]:
    full_graph_rep_tensor = torch.cat([full_graph_rep_tensor, tensor], dim=0)
#import pdb;pdb.set_trace()
for topic in tqdm(topics):    

    query_id = topic["query_id"]
    NTCIR_ID = topic["NTCIR_ID"]
    index_full_torch = int(topic["index"])

    query_rep = full_graph_rep_tensor[index_full_torch]
    cos_tensor = torch.cosine_similarity(query_rep, full_graph_rep_tensor, dim=-1)
    values, indices = torch.topk(cos_tensor, top_k, largest=True, sorted=True)
    #import pdb; pdb.set_trace()
    for j in range(top_k):        
        try:
            A = ids_formulas[indices[j]]
            B = str(values[j].item())
            temp = NTCIR_ID + ' xxx ' + A + ' ' + str(j+1) + ' ' + B + ' ' + "slt_no_merge_100_epoch"
        except:
            import pdb; pdb.set_trace()
        t.write(temp + '\n')    
t.close()