from torch_geometric.data import Data
from glob import glob
import numpy as np
import torch
import os
from tqdm import tqdm
import argparse
import json

def get_ntcir_name(id, queries):
    actual_query = {}
    for query in queries.values():
        if query["id"] == id:
            actual_query = query
            break
    ntcir_id = f"NTCIR12-MathWiki-{actual_query['NTCIR_ID'][:-5]}"
    return ntcir_id
    


parser = argparse.ArgumentParser()
parser.add_argument('-n','--namespace', type=str)
args = parser.parse_args()

namespace = args.namespace

#data_path = f"{namespace}/individual/*"
data_path = os.path.join(namespace,"individual/*.npz")

#destination = f"{namespace}/torch_data"
destination = os.path.join(namespace, "torch_data")

with open(os.path.join(namespace,"symbols_frequency_index.json")) as f:
    symbols_frequency_index = json.load(f)

with open(os.path.join(namespace,"symbols_index.json")) as f:
    symbols_index = json.load(f)
symbols_index_invert = {v: k for k, v in symbols_index.items()}

allowable_features = list(filter(lambda x: symbols_frequency_index[x]>=5, symbols_frequency_index.keys()))
allowable_features.sort()
allowable_features = ["DEFAULT"] +allowable_features

#import pdb;pdb.set_trace()

if not os.path.exists(destination):
    os.makedirs(destination)

list_data = []


topics_meta_retrieval = []
count = 0

temp = glob(data_path)
temp.sort()

for file in tqdm(temp):
    #import pdb;pdb.set_trace()
    id = os.path.basename(file)[:-4]    
    npzfile = np.load(file)
    nodes = npzfile["nodes"]
    edges_numpy = npzfile["edges_numpy"]

    _x = torch.tensor(np.array(nodes)
            .reshape((len(nodes),1)), dtype=torch.long)
    x = torch.tensor(np.vectorize(lambda y: allowable_features.index(symbols_index_invert[y]))(_x))
        
    
    if edges_numpy.size > 0:
        edge_index = torch.tensor(np.transpose(edges_numpy[:,:2]), dtype=torch.long)
        edge_attr = torch.tensor(edges_numpy[:,2].reshape(len(edges_numpy),1), dtype=torch.long)
    else:
        edge_index = torch.empty((2, 0), dtype=torch.long)
        edge_attr = torch.empty((0, 1), dtype=torch.long)

    data = Data(x=x, edge_index=edge_index, edge_attr=edge_attr, formula_id=id)
    
    list_data.append(data)
    torch.save(data, os.path.join(destination, f"{id}.pt"))
    count+=1

torch.save(list_data, os.path.join(namespace, "full.pt"))