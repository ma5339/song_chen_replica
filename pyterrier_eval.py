import pyterrier as pt
import os
import pandas as pd
from pyterrier.measures import *
from glob import glob

pt.init()
print("judge.txt")
qrels = pd.read_csv('result_old/judge.txt', sep=' ',names=["TOPIC_ID", "xxx", "FORMULA_ID", "RELEVANCE"])
topics = pd.read_csv('data/NTCIR-12_MathIR_Wikipedia_Corpus/topics.tsv',header=0, sep='\t')[["FORMULA_ID","LATEX"]]

#NTCIR12-MathWiki-1 xxx Prime_zeta_function:26 2 1.0 gcn_5000_w2c
SONG_CHEN = pd.read_csv('result_old/result_gcn_5000_w2c.txt',
                        names=["TOPIC_ID","xxx","FORMULA_ID","RANK","SCORE","RUN"], sep=' ')
#result_old/tangent_cft.txt
TANGENT = pd.read_csv('result_old/tangent_cft.txt',
                        names=["TOPIC_ID","xxx","FORMULA_ID","RANK","SCORE","RUN"], sep=' ')

SONG_CHEN_REPLICATION = pd.read_csv('result_old/baseline_test_new_trained.txt',
                        names=["TOPIC_ID","xxx","FORMULA_ID","RANK","SCORE","RUN"], sep=' ')


qrels_pt = qrels.copy().drop(columns=['xxx']).rename(columns={"TOPIC_ID":"qid",
                                                              "FORMULA_ID":"docno", "RELEVANCE":"label"}).astype({"label":'int32'})
topics_pt = topics.copy().rename(columns={"FORMULA_ID":"qid", "LATEX":"query"})
#“qid”, “docno”, “score”, “rank”

SONG_CHEN_pt = SONG_CHEN.copy().drop(columns=['RUN','xxx']).rename(columns={"TOPIC_ID":"qid",
                                                                                     "FORMULA_ID":"docno","SCORE":"score","RANK":"rank"})

TANGENT = TANGENT.copy().drop(columns=['RUN','xxx']).rename(columns={"TOPIC_ID":"qid",
                                                                                     "FORMULA_ID":"docno","SCORE":"score","RANK":"rank"})

SONG_CHEN_REPLICATION = SONG_CHEN_REPLICATION.copy().drop(columns=['RUN','xxx']).rename(columns={"TOPIC_ID":"qid",
                                                                                     "FORMULA_ID":"docno","SCORE":"score","RANK":"rank"})

#import pdb;pdb.set_trace()
runs = [SONG_CHEN_pt, TANGENT, SONG_CHEN_REPLICATION]
names = ["SONG_CHEN_pt","tangentcft", "SONG_CHEN_REPLICATION"]

for file in glob("result_final/*"):
    name = os.path.basename(file)
    current = pd.read_csv(file,
                        names=["TOPIC_ID","FORMULA_ID","FORMULA_ID_2","RANK","SCORE","RUN"], sep='\t')
    current_pt = current.copy().drop(columns=['RUN','FORMULA_ID_2']).rename(columns={"TOPIC_ID":"qid",
                                                                                     "FORMULA_ID":"docno","SCORE":"score","RANK":"rank"})
    runs.append(current_pt)
    names.append(name)
a = pt.Experiment(
    runs,
    topics_pt,
    qrels_pt,
    eval_metrics=[nDCG@1000, Bpref, P@10, P@5,P@1],
    names=names
)

print(a)


'''
TOPICS
dataset.get_topics('title')
'''