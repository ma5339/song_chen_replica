import csv

graph_types = ["SLT_MERGE", "OPT_MERGE", "SLT_NO_MERGE",
                               "OPT_NO_MERGE", "SLT_OPT_MERGE", "SLT_OPT_NO_MERGE"]

configs_listas = [0,110,121,132,143,154,165,176,187,198,28,39,425,4,60,71,82,93,100,111,122,133,144,155,166,177,188,199,29,3,42,50,61,72,83,94,101,112,123,134,145,156,167,178,189,19,2,400,436,51,62,73,84,95,102,113,124,135,146,157,168,179,18,1,30,401,437,52,63,74,85,96,103,114,125,136,147,158,169,17,190,20,31,402,438,53,64,75,86,97,104,115,126,137,148,159,16,180,191,21,32,40,43,54,65,76,87,98,105,116,127,138,149,15,170,181,192,22,33,412,44,55,66,77,88,99,106,117,128,139,14,160,171,182,193,23,34,413,45,56,67,78,89,9,107,118,129,13,150,161,172,183,194,24,35,414,46,57,68,79,8,108,119,12,140,151,162,173,184,195,25,36,41,47,58,69,7,90,109,11,130,141,152,163,174,185,196,26,37,423,48,59,6,80,91,10,120,131,142,153,164,175,186,197,27,38,424,49,5,70,81,92] 

def retrieval():
    pinga = []
    count = -1
    temporal = []
    for pooling in ["sum", "mean", "max"]:#--context_pooling
        for aggregation in ["last", "sum", "max"]:#--JK
            for decay in [0, 0.1, 0.001]:#--decay
                for dropout in [0,0.2,0.4]:
                    for graph in graph_types:
                        count+=1
                        if count in configs_listas:
                            continue
                        name = f"GRID_{count}_"
                        command1 = f"python3 graph_rep.py --JK {aggregation} --dataset data_from_old/grid_arqmath1_3_{graph} --input_model_file model/{name}.pth --tail {name}grid --device <DEVICE>"
                        command2 = f"python3 graph_rep.py --JK {aggregation} --dataset data_from_old/ARQMath1Topics_{graph}_prebuilt --input_model_file model/{name}.pth --tail {name}topics --device <DEVICE>"
                        command3 = f"python3 retrieval_main.py -d graph_rep/{name}grid -p graph_rep/{name}topics -n {name}"
                        temporal.append(f"{command1}\n{command2}\n{command3}")                     
                        #print(command)
                        
                        if len(temporal)==50:
                            pinga.append(temporal)
                            temporal = []
    if len(temporal)>0:
        pinga.append(temporal)        
    print(count)
    for _idx,i in enumerate(["BOB_0_retri.sh","BOB_1_retri.sh","BOB_2_retri.sh","BOB_3_retri.sh","DOUG_0_retri.sh","DOUG_1_retri.sh","DOUG_2_retri.sh","DOUG_3_retri.sh", "bobHAL_0_retri.sh","dougHAL_1_retri.sh"]):
        with open(f"grids_retrieval_commands_gpus/{i}", "w") as file:
            #import pdb;pdb.set_trace()
            for line in pinga[_idx]:
                device = i.split("_")[1].split(".")[0]
                #<DEVICE>
                towrite = f"{line}\n\n"
                file.write(towrite.replace("<DEVICE>",device))

#python3 graph_rep.py --JK last --num_node_label 1648 --num_edge_label 56 --initial_node emb --input_model_file model/TUNN_0_1648.pth --dataset data_from_old/ARQMath_1-3_qrels_tunning/full.pt --tail TUNN_0_ARQMath_1-3_qrels_tunning
#python3 graph_rep.py --JK last --num_node_label 1648 --num_edge_label 56 --initial_node emb --input_model_file model/TUNN_0_1648.pth --dataset data_from_old/ARQMath1_topics_OPT_MERGE_from_prebuilt_index/full.pt --tail TUNN_0_ARQMath1_topics_OPT_MERGE
#python3 retrieval_main.py -d graph_rep/TUNN_0_ARQMath_1-3_qrels_tunning -p graph_rep/TUNN_0_ARQMath1_topics_OPT_MERGE -k 1000 -n TUNN_0_



def for_training():
    pinga = []
    count = -1
    temporal = []
    for pooling in ["sum", "mean", "max"]:#--context_pooling
        for aggregation in ["last", "sum", "max"]:#--JK
            for decay in [0, 0.1, 0.001]:#--decay
                for dropout in [0,0.2,0.4]:
                    for graph in graph_types:
                        count+=1
                        if count in configs_listas:
                            continue
                        params = f"--context_pooling {pooling} --JK {aggregation} --decay {decay} --dropout_ratio {dropout}"
                        name = f"GRID_{count}_"
                        command = f"python3 pretrain.py --dataset data_from_old/grid_arqmath1_3_{graph} --epochs 100 --name {name} {params}"   
                        temporal.append(command)                     
                        #print(command)
                        
                        if len(temporal)==16:
                            pinga.append(temporal)
                            temporal = []
    if len(temporal)>0:
        pinga.append(temporal)        
    print(count)
    for _idx,i in enumerate(["BOB_A_0.sh","BOB_A_1.sh","BOB_A_2.sh","BOB_A_3.sh","DOUG_A_0.sh","DOUG_A_1.sh","DOUG_A_2.sh","DOUG_A_3.sh",
                             "BOB_B_0.sh","BOB_B_1.sh","BOB_B_2.sh","BOB_B_3.sh","DOUG_B_0.sh","DOUG_B_1.sh","DOUG_B_2.sh","DOUG_B_3.sh",
                              "NIA_A_0.sh","NIA_A_1.sh"]):
        with open(f"grids_commands_remake_gpus/{i}", "w") as file:
            #import pdb;pdb.set_trace()
            for line in pinga[_idx]:
                device = i.split("_")[2].split(".")[0]
                file.write(f"{line} --device {device}\n")
    
for_training()

'''
for graph in graph_types:
    #a = f"python3 format_song_chen.py -r data/raw/NTCIR-12_MathIR_Wikipedia_Corpus/NoTopicsNoDuplicates -p grid_ntcir_{graph} -g {graph}"
    a = f"python3 convert_numpy_to_torch.py -n data_from_old/grid_arqmath1_3_{graph}/"
    print(a)
    
    print()
'''
#python3 pretrain.py --dataset data_from_old/grid_arqmath1_3_OPT_MERGE --epochs 5 --name test_here