from torch_geometric.data import Data
from glob import glob
import numpy as np
import torch
import os
from tqdm import tqdm
import argparse
import json


parser = argparse.ArgumentParser()
parser.add_argument('-n','--namespace', type=str)
args = parser.parse_args()

namespace = args.namespace

data_path = f"{namespace}/topics_arqmath/*"

destination = f"{namespace}/topics_arqmath/"

if not os.path.exists(destination):
    os.makedirs(destination)

list_data = []

topics_meta_retrieval = []
count = 0
temp = glob(data_path)
temp.sort()
for file in tqdm(temp):
    if count==0:
        count+=1
        continue
    print(file)
    #import pdb;pdb.set_trace()
    id = os.path.basename(file)[:-4]    
    npzfile = np.load(file)
    nodes = npzfile["nodes"]
    edges_numpy = npzfile["edges_numpy"]

    x = torch.tensor(np.array(nodes)
            .reshape((len(nodes),1)), dtype=torch.long)
    
    if edges_numpy.size > 0:
        edge_index = torch.tensor(np.transpose(edges_numpy[:,:2]), dtype=torch.long)
        edge_attr = torch.tensor(edges_numpy[:,2].reshape(len(edges_numpy),1), dtype=torch.long)
    else:
        edge_index = torch.empty((2, 0), dtype=torch.long)
        edge_attr = torch.empty((0, 1), dtype=torch.long)

    data = Data(x=x, edge_index=edge_index, edge_attr=edge_attr)
    print(id,data)
    list_data.append(data)
    #torch.save(data, os.path.join(destination, f"{id}.pt"))
    count+=1
    break

torch.save(list_data, os.path.join(namespace, "full_topics_arqmath.pt"))