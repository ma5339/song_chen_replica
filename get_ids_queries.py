
from glob import glob
import os
import json

query_list = ['Prime_zeta_function_28.txt',
'Codex_Sinaiticus_1.txt',
'Word_lists_by_frequency_1.txt',
'On_Physical_Lines_of_Force_0.txt',
"Backhouse's_constant_0.txt",
'Unbinilium_1.txt',
'Extension_of_a_topological_group_38.txt',
'Nowcast_(Air_Quality_Index)_1.txt',
'Generator_(circuit_theory)_0.txt',
'Lerch_zeta_function_0.txt',
'Monic_polynomial_1.txt',
'Hunt–McIlroy_algorithm_2.txt',
'Mathematical_morphology_24.txt',
'Hyperbolic_law_of_cosines_2.txt',
'Strong_antichain_0.txt',
'Delay_spread_2.txt',
'Goldbach–Euler_theorem_7.txt',
'Statistical_coupling_analysis_1.txt',
'Anisotropic_Network_Model_4.txt',
'Correlation_and_dependence_1.txt']


namespace = "data_from_old/full_slt_no_merge_with_topics"

path = f"{namespace}/individual/*"
PENDEJO = [{
    "file_id":"Codex_Sinaiticus:1",
    "NTCIR_id":"NTCIR12-MathWiki-2" 
},
{
    "file_id":"Unbinilium:1",
    "NTCIR_id":"NTCIR12-MathWiki-5" 
}]
for index,file in enumerate(glob(path)):
    #import pdb;pdb.set_trace()
    formula_id = os.path.basename(file)[:-4]
    try:        
        html_name = formula_id[:formula_id.rfind(":")]
        idx = formula_id[formula_id.rfind(":")+1:]
    except:
        import pdb;pdb.set_trace()
    name = f"{html_name}_{idx}.txt"
    if name in query_list:
        id_in_list = query_list.index(name)
        
        print(formula_id,"------",index,"*****",id_in_list)
        PENDEJO.append({
            "file_id":formula_id,
            "NTCIR_id":f"NTCIR12-MathWiki-{id_in_list+1}",
            "index_full_torch":index
        })

with open("topics_meta.json", 'w+') as file:
    file.write(json.dumps(PENDEJO))
    