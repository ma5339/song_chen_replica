python3 graph_rep.py --JK sum --dataset data_from_old/grid_arqmath1_3_SLT_MERGE --input_model_file model/GRID_264_.pth --tail GRID_264_grid --device 0
python3 graph_rep.py --JK sum --dataset data_from_old/ARQMath1Topics_SLT_MERGE_prebuilt --input_model_file model/GRID_264_.pth --tail GRID_264_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_264_grid -p graph_rep/GRID_264_topics -n GRID_264_

python3 graph_rep.py --JK sum --dataset data_from_old/grid_arqmath1_3_OPT_MERGE --input_model_file model/GRID_265_.pth --tail GRID_265_grid --device 0
python3 graph_rep.py --JK sum --dataset data_from_old/ARQMath1Topics_OPT_MERGE_prebuilt --input_model_file model/GRID_265_.pth --tail GRID_265_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_265_grid -p graph_rep/GRID_265_topics -n GRID_265_

python3 graph_rep.py --JK sum --dataset data_from_old/grid_arqmath1_3_SLT_NO_MERGE --input_model_file model/GRID_266_.pth --tail GRID_266_grid --device 0
python3 graph_rep.py --JK sum --dataset data_from_old/ARQMath1Topics_SLT_NO_MERGE_prebuilt --input_model_file model/GRID_266_.pth --tail GRID_266_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_266_grid -p graph_rep/GRID_266_topics -n GRID_266_

python3 graph_rep.py --JK sum --dataset data_from_old/grid_arqmath1_3_OPT_NO_MERGE --input_model_file model/GRID_267_.pth --tail GRID_267_grid --device 0
python3 graph_rep.py --JK sum --dataset data_from_old/ARQMath1Topics_OPT_NO_MERGE_prebuilt --input_model_file model/GRID_267_.pth --tail GRID_267_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_267_grid -p graph_rep/GRID_267_topics -n GRID_267_

python3 graph_rep.py --JK sum --dataset data_from_old/grid_arqmath1_3_SLT_OPT_MERGE --input_model_file model/GRID_268_.pth --tail GRID_268_grid --device 0
python3 graph_rep.py --JK sum --dataset data_from_old/ARQMath1Topics_SLT_OPT_MERGE_prebuilt --input_model_file model/GRID_268_.pth --tail GRID_268_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_268_grid -p graph_rep/GRID_268_topics -n GRID_268_

python3 graph_rep.py --JK sum --dataset data_from_old/grid_arqmath1_3_SLT_OPT_NO_MERGE --input_model_file model/GRID_269_.pth --tail GRID_269_grid --device 0
python3 graph_rep.py --JK sum --dataset data_from_old/ARQMath1Topics_SLT_OPT_NO_MERGE_prebuilt --input_model_file model/GRID_269_.pth --tail GRID_269_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_269_grid -p graph_rep/GRID_269_topics -n GRID_269_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_SLT_MERGE --input_model_file model/GRID_270_.pth --tail GRID_270_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_SLT_MERGE_prebuilt --input_model_file model/GRID_270_.pth --tail GRID_270_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_270_grid -p graph_rep/GRID_270_topics -n GRID_270_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_OPT_MERGE --input_model_file model/GRID_271_.pth --tail GRID_271_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_OPT_MERGE_prebuilt --input_model_file model/GRID_271_.pth --tail GRID_271_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_271_grid -p graph_rep/GRID_271_topics -n GRID_271_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_SLT_NO_MERGE --input_model_file model/GRID_272_.pth --tail GRID_272_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_SLT_NO_MERGE_prebuilt --input_model_file model/GRID_272_.pth --tail GRID_272_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_272_grid -p graph_rep/GRID_272_topics -n GRID_272_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_OPT_NO_MERGE --input_model_file model/GRID_273_.pth --tail GRID_273_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_OPT_NO_MERGE_prebuilt --input_model_file model/GRID_273_.pth --tail GRID_273_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_273_grid -p graph_rep/GRID_273_topics -n GRID_273_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_SLT_OPT_MERGE --input_model_file model/GRID_274_.pth --tail GRID_274_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_SLT_OPT_MERGE_prebuilt --input_model_file model/GRID_274_.pth --tail GRID_274_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_274_grid -p graph_rep/GRID_274_topics -n GRID_274_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_SLT_OPT_NO_MERGE --input_model_file model/GRID_275_.pth --tail GRID_275_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_SLT_OPT_NO_MERGE_prebuilt --input_model_file model/GRID_275_.pth --tail GRID_275_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_275_grid -p graph_rep/GRID_275_topics -n GRID_275_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_SLT_MERGE --input_model_file model/GRID_276_.pth --tail GRID_276_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_SLT_MERGE_prebuilt --input_model_file model/GRID_276_.pth --tail GRID_276_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_276_grid -p graph_rep/GRID_276_topics -n GRID_276_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_OPT_MERGE --input_model_file model/GRID_277_.pth --tail GRID_277_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_OPT_MERGE_prebuilt --input_model_file model/GRID_277_.pth --tail GRID_277_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_277_grid -p graph_rep/GRID_277_topics -n GRID_277_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_SLT_NO_MERGE --input_model_file model/GRID_278_.pth --tail GRID_278_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_SLT_NO_MERGE_prebuilt --input_model_file model/GRID_278_.pth --tail GRID_278_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_278_grid -p graph_rep/GRID_278_topics -n GRID_278_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_OPT_NO_MERGE --input_model_file model/GRID_279_.pth --tail GRID_279_grid --device 0
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_OPT_NO_MERGE_prebuilt --input_model_file model/GRID_279_.pth --tail GRID_279_topics --device 0
python3 retrieval_main.py -d graph_rep/GRID_279_grid -p graph_rep/GRID_279_topics -n GRID_279_

