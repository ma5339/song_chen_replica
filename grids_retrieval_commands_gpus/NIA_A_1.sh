python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_SLT_OPT_MERGE --input_model_file model/GRID_484_.pth --tail GRID_484_grid --device 1
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_SLT_OPT_MERGE_prebuilt --input_model_file model/GRID_484_.pth --tail GRID_484_topics --device 1
python3 retrieval_main.py -d graph_rep/GRID_484_grid -p graph_rep/GRID_484_topics -n GRID_484_

python3 graph_rep.py --JK max --dataset data_from_old/grid_arqmath1_3_SLT_OPT_NO_MERGE --input_model_file model/GRID_485_.pth --tail GRID_485_grid --device 1
python3 graph_rep.py --JK max --dataset data_from_old/ARQMath1Topics_SLT_OPT_NO_MERGE_prebuilt --input_model_file model/GRID_485_.pth --tail GRID_485_topics --device 1
python3 retrieval_main.py -d graph_rep/GRID_485_grid -p graph_rep/GRID_485_topics -n GRID_485_

